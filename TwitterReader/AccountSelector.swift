//
//  AccountSelectorView.swift
//  TwitterReader
//
//  Created by Nikita on 19.10.18.
//  Copyright © 2018 Nikita Feshchun. All rights reserved.
//

import Foundation

public class AccountSelector
{
    let view = UIStackView()
    let textField = UITextField()
    let button = UIButton()
    
    let reloadListener: (String) -> Void
    
    init(_ reloadListener: @escaping (String) -> Void) {
        self.reloadListener = reloadListener
        prepareView()
    }
    
    func prepareView()
    {
        configureView()
        configureTextField()
        configureButton()
    }
    
    func configureView()
    {
        view.axis = .horizontal
        view.addArrangedSubview(textField)
        view.addArrangedSubview(button)
        view.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func configureTextField()
    {
        textField.placeholder = "Account"
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        textField.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        textField.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        textField.rightAnchor.constraint(equalTo: button.leftAnchor).isActive = true
    }
    
    func configureButton()
    {
        button.setTitle("Reload", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.widthAnchor.constraint(equalToConstant: 80).isActive = true
        button.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        button.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        button.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
        button.addTarget(self, action: #selector(AccountSelector.buttonClicked(_:)), for: .touchUpInside)
    }
    
    @objc func buttonClicked(_ button: UIButton) {
        reloadListener(textField.text ?? "")
    }
}
