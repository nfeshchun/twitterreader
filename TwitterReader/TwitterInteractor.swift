//
//  TwitterInteractor.swift
//  TwitterReader
//
//  Created by Nikita on 18.10.18.
//  Copyright © 2018 Nikita Feshchun. All rights reserved.
//

import Foundation
import UIKit
import WebKit

public class TwitterInteractor
{
    var webview: WKWebView?
    var controlView: UIButton?
    lazy var accountSelector: AccountSelector = AccountSelector({ [unowned self] twitterAccount in
        self.prepareSource(for: twitterAccount)
    })
    
    public init()
    {
        
    }
    
    public func atachTL(for view: UIView)
    {
        prepareViews(for: view)
        setupWebview()
        setupControlView(with: view)
        setupAccountView()
    }
    
    private func prepareViews(for view: UIView)
    {
        webview = WKWebView()
        controlView = UIButton()
        
        guard let webview = webview, let controlView = controlView
            else
        {
            return
        }
        
        controlView.addSubview(accountSelector.view)
        controlView.addSubview(webview)
        view.addSubview(controlView)
        
        controlView.isUserInteractionEnabled = true
        controlView.backgroundColor = UIColor.gray.withAlphaComponent(0.5)
        controlView.addTarget(self, action: #selector(closeView(_:)), for: .touchUpInside)
        
    }
    
    @objc func closeView(_ button: UIButton) {
        // handling code
        controlView?.removeFromSuperview()
        controlView = nil
        webview = nil
    }
    
    private func setupAccountView()
    {
        guard let controlView = controlView
            else
        {
            return
        }
        accountSelector.view.translatesAutoresizingMaskIntoConstraints = false
        accountSelector.view.leftAnchor.constraint(equalTo: controlView.leftAnchor).isActive = true
        accountSelector.view.topAnchor.constraint(equalTo: controlView.topAnchor, constant: 20).isActive = true
        accountSelector.view.heightAnchor.constraint(equalToConstant: 48.0).isActive = true
        accountSelector.view.widthAnchor.constraint(equalTo: controlView.widthAnchor, multiplier: 0.5).isActive = true
    }
    
    private func setupWebview()
    {
        guard let webview = webview, let controlView = controlView
            else
        {
            return
        }
        webview.translatesAutoresizingMaskIntoConstraints = false
        webview.leftAnchor.constraint(equalTo: controlView.leftAnchor).isActive = true
        webview.topAnchor.constraint(equalTo: accountSelector.view.bottomAnchor).isActive = true
        webview.bottomAnchor.constraint(equalTo: controlView.bottomAnchor).isActive = true
        webview.widthAnchor.constraint(equalTo: controlView.widthAnchor, multiplier: 0.5).isActive = true
    }
    
    private func setupControlView(with view: UIView)
    {
        guard let controlView = controlView
            else
        {
            return
        }
        controlView.translatesAutoresizingMaskIntoConstraints = false
        controlView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        controlView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        controlView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        controlView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
    }
    
    public func prepareSource(for user: String)
    {
        let responseTwitterHTMLContent = "<a class=\"twitter-timeline\" href=\"https://twitter.com/\(user)?ref_src=twsrc%5Etfw\">Tweets by TwitterDev</a> <script async src=\"https://platform.twitter.com/widgets.js\" charset=\"utf-8\"></script> "
        webview?.loadHTMLString(responseTwitterHTMLContent, baseURL: nil)
    }
}
